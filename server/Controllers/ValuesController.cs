﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalRChat.Hubs;
using Newtonsoft.Json;
using server.Models;

namespace server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionsController : ControllerBase
    {
        IHubContext<ChatHub> _hub;
        public QuestionsController(IHubContext<ChatHub> hub)
        {
            _hub = hub;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // POST api/values
        [Route("comment")]
        [HttpPost]
        public async void Post([FromBody] Comment comment)
        {
            await _hub.Clients.All.SendAsync("Comment", "user", JsonConvert.SerializeObject(comment));
        }

        [Route("question")]
        [HttpPost]
        public async void Question([FromBody] Question question)
        {
            if (question == null || question.Id == 0)
                await _hub.Clients.All.SendAsync("Question", "user", "");
            else
                await _hub.Clients.All.SendAsync("Question", "user", JsonConvert.SerializeObject(question));
        }


        [Route("answer")]
        [HttpPost]
        public async void Answers([FromBody] Answer answer)
        {
            await _hub.Clients.All.SendAsync("Answer", "user", JsonConvert.SerializeObject(answer));
        }

    }
}
