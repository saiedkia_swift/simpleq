using System;


namespace server.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string One { get; set; }
        public string Two { get; set; }
        public string Three { get; set; }
        public string Four { get; set; }

        public string Text {get; set;}
    }


    public class Comment
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
    }

    public class Answer{
        public Question Question { get;set;}
        public int CurrectAnswerIndex {get;set;}
        public int OptionOne {get;set;}
        public int OptionTwo{get;set;}
        public int OptionThree{get;set;}
        public int OptionFour{get;set;}
    }
}