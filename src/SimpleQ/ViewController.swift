//
//  ViewController.swift
//  SimpleQ
//
//  Created by sk on 4/11/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SwiftSignalRClient
import SwiftyJSON

class ViewController: BaseViewController {
    
    
    @IBOutlet weak var vPlayer: UIView!
    @IBOutlet weak var vQuestion: UIView!
    @IBOutlet weak var vComments: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UIView!
    
    
    private var player:AVPlayer!
    private var palyerView:AVPlayerLayer!
    private var vController:AVPlayerViewController!
    private var comments = [Comment]()
    private var hubConnectionBuilder:HubConnection?
    private var qHubConnectionDelegate:QHubConnectionDelegate?
    private var textFiled:CustomeTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        vQuestion.alpha = 0
        //vComments.alpha = 0
        
        tableView.register(UINib(nibName: "CommentRowItem", bundle: nil), forCellReuseIdentifier: "CommentRowItem")
        tableView.delegate = self
        tableView.dataSource = self
        
        textFiled = (Bundle.main.loadNibNamed("CustomeTextField", owner: nil, options: nil)?[0] as! CustomeTextField)
        textFiled.frame = textView.bounds
        textView.addSubview(textFiled)
        
        textFiled.sendDelegate = {(newValue) in
            if newValue == "" { return }
            
            let net = Network()
            let data = [
                "id" : "123",
                "userName" : "saiedKia",
                "text" : newValue
            ]
            
            self.textFiled.text = ""
            net.post(route: "http://localhost:5000/api/questions/comment", body: data, callback: {(json, status) in })
            
                 NotificationCenter.default.post(name: Notification.Name("dissmissKeyboard"), object: nil)
                NotificationCenter.default.post(name: Notification.Name("dissmissKeyboard"), object: nil)
        }
        
        textFiled.setupAccessoryView()
    }
    
    

    
    
    
    private func NextQuestion(data:String){
        if data == ""{
            vQuestion.alpha = 0
            return
        }else{
            let json = JSON(parseJSON: data)
            
            if json["One"].string != nil{
                vQuestion.alpha = 1
                vQuestion.clearSubViews()
                
                let questionBox = UINib(nibName: "QuestionBoxView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! QuestionBoxView
                
                questionBox.frame = vQuestion.bounds
                vQuestion.addSubview(questionBox)
                
                questionBox.setData(question: json["Text"].stringValue, one: json["One"].stringValue, two: json["Two"].stringValue, three: json["Three"].stringValue, four: json["Four"].stringValue, seconds: 10)
            }
        }
    }
    
    private func ShowAnswers(data:String){
        NextQuestion(data: "")
        
        let json = JSON(parseJSON: data)
        
        if json["CurrectAnswerIndex"].int != nil{
            vQuestion.alpha = 1
            vQuestion.clearSubViews()
            
            let questionBox = UINib(nibName: "QuestionBoxView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! QuestionBoxView
            
            questionBox.frame = vQuestion.bounds
            vQuestion.addSubview(questionBox)
            
            
            let question = json["Question"]
            questionBox.setData(question: question["Text"].stringValue, one: question["One"].stringValue, two: question["Two"].stringValue, three: question["Three"].stringValue, four: question["Four"].stringValue, seconds: 0, isReadOnly: true, selectedIndex: json["CurrectAnswerIndex"].intValue)
        }
    }
    
    private func addNewComment(data:String){
        let json = JSON(parseJSON: data)
        if json["Text"].string != nil{
            comments.append(Comment(owner: json["UserName"].stringValue, text: json["Text"].stringValue))
            tableView.reloadData()
            scrollToEnd()
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        connect()
        getConductor()
    }

}


extension ViewController {
    private func getConductor(){
        
        let net = Network()
        net.get(route: "https://api.anten.ir/v3.0/channels/8/conductor", callback: {(json, status) in
            
            switch status{
            case .success:
                let id = json![0]["id"].stringValue
                net.get(route: "https://api.anten.ir/v3.0/channels/8/conductor/\(id)/episode", callback: {(json, status) in
                    
                    switch status{
                    case .success:
                        let url = json!["url"].stringValue
                        self.playVideo(url: url)
                        break
                    default:
                        self.showAlert(message: "خطایی رخ داده است", btnOneTitle: "تلاش محدد", btnTwoTitle: "بیخیال", callBackOne: {self.getConductor()}, callBackTwo: {}, cancelCallback: nil)
                        break
                    }
                }, onLoginNeeded: {})
                break
            case .internetFailed:
                self.showAlert(message: "لطفا اینترنت خود را بررسی نمایید", btnTitle: "تایید", callBack: {self.getConductor()})
                break
            case .serverError, .authenticationFailed, .failed:
                self.showAlert(message: "خطایی رخ داده است", btnOneTitle: "تلاش محدد", btnTwoTitle: "بیخیال", callBackOne: {self.getConductor()}, callBackTwo: {}, cancelCallback: nil)
                break
            }
        }, onLoginNeeded: {})
    }
    
    
    private func playVideo(url:String){
        player = AVPlayer(url: URL(string: url)!)
    
        player.allowsExternalPlayback = true
        vController = AVPlayerViewController()
        vController.showsPlaybackControls = false
        vController.player = player
        vController.view.frame = vPlayer.bounds
        player.externalPlaybackVideoGravity = .resize
        vPlayer.addSubview(vController.view)
        player.play()
    }
}



extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentRowItem") as! CommentRowItemTableViewCell
        cell.setData(text: comments[indexPath.row].text ?? "")
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    
    private func scrollToEnd(){
        if comments.count <= 1 { return }
        
        tableView.scrollToRow(at: IndexPath(row: comments.count - 1, section: 0), at: .bottom, animated: true)
    }
}


extension ViewController{
    private func connect(){
        hubConnectionBuilder = HubConnectionBuilder(url: URL(string: "http://localhost:5000/chatHub")!).withLogging(minLogLevel: .debug).build()
        
        qHubConnectionDelegate = QHubConnectionDelegate()
        hubConnectionBuilder?.delegate = qHubConnectionDelegate
        
        
        hubConnectionBuilder!.on(method: "Comment", callback: {args, typeConverter in
            //let user = try! typeConverter.convertFromWireType(obj: args[0], targetType: String.self)
            let message = try! typeConverter.convertFromWireType(obj: args[1], targetType: String.self)
            
            
            self.addNewComment(data: message ?? "")
        })
        
        hubConnectionBuilder!.on(method: "Question", callback: {args, typeConverter in
            //let user = try! typeConverter.convertFromWireType(obj: args[0], targetType: String.self)
            let message = try! typeConverter.convertFromWireType(obj: args[1], targetType: String.self)
            
            
            self.NextQuestion(data: message ?? "")
        })
        
        hubConnectionBuilder!.on(method: "Answer", callback: {args, typeConverter in
            //let user = try! typeConverter.convertFromWireType(obj: args[0], targetType: String.self)
            let message = try! typeConverter.convertFromWireType(obj: args[1], targetType: String.self)
            
            
            self.ShowAnswers(data: message ?? "")
        })
        
        hubConnectionBuilder!.start()
    }
}

class QHubConnectionDelegate: HubConnectionDelegate {
    func connectionDidOpen(hubConnection: HubConnection!) {
        //app?.connectionDidStart()
    }
    
    func connectionDidFailToOpen(error: Error) {
        //app?.connectionDidFailToOpen(error: error)
    }
    
    func connectionDidClose(error: Error?) {
        //app?.connectionDidClose(error: error)
    }
}

