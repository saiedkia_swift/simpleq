//
//  QuestionBoxView.swift
//  SimpleQ
//
//  Created by sk on 4/14/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import UIKit
import Foundation
import UICircularProgressRing

class QuestionBoxView: UIView{
    @IBOutlet weak var vTimer: UICircularTimerRing!
    @IBOutlet weak var lblQuestion: UILabel!
    
    
    @IBOutlet weak var vQuestionOne: UIView!
    @IBOutlet weak var vQuestionTwo: UIView!
    @IBOutlet weak var vQuestionThree: UIView!
    @IBOutlet weak var vQuestionFour: UIView!
    
    
    @IBOutlet weak var lblQuestionOne: UILabel!
    @IBOutlet weak var lblQuestionTwo: UILabel!
    @IBOutlet weak var lblQuestionThree: UILabel!
    @IBOutlet weak var lblQuestionFour: UILabel!
    
    private var answerIsSelectable:Bool = true
    
    private var maxSecond = 10
    private weak var vTimerView:TimerView?
    
    override func awakeFromNib() {
        iniView()
    }
    
    private func iniView(){
        vQuestionOne.addTapGestureRecognizer(action: {
            self.selectAnswer(1)
        })
        
        vQuestionTwo.addTapGestureRecognizer(action: {
            self.selectAnswer(2)
        })
        
        vQuestionThree.addTapGestureRecognizer(action: {
            self.selectAnswer(3)
        })
        
        vQuestionFour.addTapGestureRecognizer(action: {
            self.selectAnswer(4)
        })
    }
    
    private func selectAnswer(_ index:Int){
        
        if !answerIsSelectable { return }
        
        var view:UIView!
        
        switch index{
        case 1:
            view = vQuestionOne
            break
        case 2:
            view = vQuestionTwo
            break
        case 3:
            view = vQuestionThree
            break
        case 4:
            view = vQuestionFour
        default:
            break
        }
        
        answerIsSelectable = false
        view.layer.borderColor = UIColor.red.cgColor
        view.layer.borderWidth = 1
        
        
        // api call or websocke
    }
    
    
    func setData(question:String, one:String, two:String, three:String, four:String, seconds:Float, isReadOnly:Bool = false, selectedIndex:Int = 0){
        
        lblQuestionOne.text = "۱- " + one
        lblQuestionTwo.text = "۲- " + two
        lblQuestionThree.text = "۳- " + three
        lblQuestionFour.text = "۴- " + four
        lblQuestion.text = question
        
        if !isReadOnly {
            DispatchQueue.main.async {
                self.startTimer()
            }
        }else{
            vTimer.alpha = 0
            selectAnswer(selectedIndex)
        }
        
        answerIsSelectable = !isReadOnly
    }
    
    func setData(one:String, two:String, three:String, four:String, seconds:Float, optionOne:String, optionTwo:String, optionThree:String, optionFour:String, selectedIndex:Int = 0){
        
    }
    
    func startTimer(){
        vTimer.valueFormatter = UICircularRingValuePersianFormatter() //UICircularTimerRingFormatter(units: .second, style: .positional)
        
        
        vTimer.startTimer(to: 10, handler: {ringState in
            switch ringState{
                
            case .finished:
                //self.showAlert(message: "times runned out!!", btnTitle: "ok", callBack: {})
                self.answerIsSelectable = false
                break
                //            case .continued(let elapsedTime):
                //                break
                //            case .paused(let elpasedTime):
            //                break
            default:
                break
            }
        })
    }
}
