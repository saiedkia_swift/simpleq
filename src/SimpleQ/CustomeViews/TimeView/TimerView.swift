//
//  TimerView.swift
//  SimpleQ
//
//  Created by sk on 4/14/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import UIKit
import CoreGraphics

class TimerView: CardView  {


    func setData(value:Int, max:Int){
        let path = UIBezierPath(arcCenter: CGPoint(x: frame.midX, y: frame.midY), radius: frame.midX, startAngle: 90, endAngle: (3.14 * 20), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor.red.cgColor
        //you can change the line width
        shapeLayer.lineWidth = 3.0
    
        layer.addSublayer(shapeLayer)
    }

}
