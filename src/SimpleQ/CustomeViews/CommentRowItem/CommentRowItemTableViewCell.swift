//
//  CommentRowItemTableViewCell.swift
//  SimpleQ
//
//  Created by sk on 4/12/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import UIKit

class CommentRowItemTableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var comment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(text:String){
        comment.text = text
    }

}
