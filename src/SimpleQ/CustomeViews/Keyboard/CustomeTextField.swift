//
//  KeyboardTextField.swift
//  SimpleQ
//
//  Created by sk on 4/22/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import UIKit

class CustomeTextField: UIView, UITextFieldDelegate {
    
    @IBOutlet weak var textField: UITextField!
    var sendDelegate:((_:String) -> Void)?
    
    private var accessoryView:CustomeTextField?
    
    var text:String{
        get {
            return textField.text ?? ""
        }
        set{
            textField.text = newValue
        }
    }
    
    override func awakeFromNib() {
        textField.delegate = self
        
        let imgSend = UIImage.init(named: "icons8-paper-plane-80")
        let uiImage = UIImageView(image: imgSend)
        
        uiImage.frame = CGRect(x: CGFloat(35), y: textField.frame.height/2, width: CGFloat(35), height: (textField.frame.height / 2) - 5)
        textField.rightView = uiImage
        textField.rightViewMode = .always
        
        uiImage.addTapGestureRecognizer(action: {
            self.sendDelegate?(self.text)
            self.text = ""
        })
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
    }
    
    func setupAccessoryView(){
        accessoryView  = (Bundle.main.loadNibNamed("CustomeTextField", owner: nil, options: nil)?[0] as! CustomeTextField)
        accessoryView?.sendDelegate = sendDelegate
        
        textField.inputAccessoryView = accessoryView
    }
    
    @objc private func keyboardWillShow(sender: NSNotification) {
        textField.becomeFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
