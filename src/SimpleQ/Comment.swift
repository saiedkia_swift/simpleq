//
//  Comment.swift
//  SimpleQ
//
//  Created by sk on 4/12/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation


class Comment{
    var owner:String?
    var text:String?
    
    init(){
    }
    
    init(owner:String, text:String){
        self.owner = owner
        self.text = text
    }
}
