//
//  UICircularRingValuePersianFormatter.swift
//  SimpleQ
//
//  Created by sk on 4/26/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation
import UICircularProgressRing


struct UICircularRingValuePersianFormatter : UICircularRingValueFormatter
{
    public func string(for value: Any) -> String? {
        return Int(value as! CGFloat).description.replacedEnglishDigitsWithEPersian
    }
}
