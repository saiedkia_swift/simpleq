//
//  CardView.swift
//  SimpleQ
//
//  Created by sk on 4/14/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import UIKit

@IBDesignable
class CardView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 1
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.2
    @IBInspectable var maskToBound : Bool = false
    @IBInspectable var topRight: Bool = true
    @IBInspectable var topLeft: Bool = true
    @IBInspectable var bottomRight: Bool = true
    @IBInspectable var bottomLeft: Bool = true
    @IBInspectable var roudCorner:Bool = false
    @IBInspectable var borderWidth:CGFloat = 0
    @IBInspectable var borderColor:UIColor?
    
    
    override func layoutSubviews() {
        layer.masksToBounds = maskToBound
        
        if roudCorner {
            layer.cornerRadius = self.frame.height / 2
        }else{
            layer.cornerRadius = cornerRadius
        }
        
        var mask = CACornerMask()
        if topLeft { mask.insert(.layerMinXMinYCorner) }
        if topRight { mask.insert(.layerMaxXMinYCorner) }
        if bottomLeft { mask.insert(.layerMinXMaxYCorner) }
        if bottomRight { mask.insert(.layerMaxXMaxYCorner) }
        
        layer.maskedCorners = mask
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor?.cgColor
    }
}


@IBDesignable
class RoundCornerView: UIView{
    @IBInspectable var topRightRadious:CGFloat = 1
    @IBInspectable var topLeftRadious:CGFloat = 1
    @IBInspectable var BottomRightRadious:CGFloat = 1
    @IBInspectable var ButtomLeftRadious:CGFloat = 1
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.2
    @IBInspectable var maskToBound : Bool = false
    
    override func layoutSubviews() {
        layer.masksToBounds = maskToBound

        let maskPath = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft, .topRight, .bottomLeft, .bottomRight],
                                     cornerRadii: CGSize(width: 1, height: 2))
        
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
        
        
        
//        layer.shadowColor = shadowColor?.cgColor
//        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
//        layer.shadowOpacity = shadowOpacity
//        layer.shadowPath = maskPath.cgPath
    }
}

