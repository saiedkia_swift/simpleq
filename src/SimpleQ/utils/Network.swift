//
//  Network.swift
//  SimpleQ
//
//  Created by sk on 4/19/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit
import AlamofireActivityLogger

class Network {
    typealias CallBack = ((JSON?, Bool) -> Void)?
    typealias GetCallback = ((JSON?, ResponseStatus) -> Void)?
    private var header:HTTPHeaders?
    
    init(){
        setContentType(.JSON)
    }
    
    func setHeaders(headers:Dictionary<String, String>, contentType:ContentType = .JSON){
        self.header = headers
        self.setContentType(contentType)
    }
    
    func addHeader(key:String, value:String){
        if header == nil {
            header = [key : value]
        }else{
            header![key] = value
        }
    }
    
    func setContentType(_ contentType:ContentType){
        var strType:String
        switch contentType{
        case .JSON:
            strType = "application/json"
            break
        case .UrlEncoded:
            strType = "application/x-www-form-urlencoded"
            break
        case .Multipart:
            strType = "multipart/form-data"
            break
        }
        
        if header == nil {
            header = ["Content-Type" : strType]
        }else{
            header!["Content-Type"] = strType
        }
    }
    
    func put(route: String, body: Dictionary<String, Any>, callback: CallBack)
    {
        guard let url = URL(string: route) else{
            print("invalid URL, put: \(route)")
            return
        }
        
        Alamofire.request(url, method:.put,parameters: body,encoding: JSONEncoding.default , headers: header)
            .log(level: .all)
            .validate().responseJSON { response in
                switch response.result {
                case .success( _):
                    callback?(JSON(response.result.value ?? ""), true)
                case .failure( _):
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        print("Failure Response: \(json!)")
                        callback?(JSON(response.data!),false)
                    }
                }
        }
    }
    
    
    func post(route: String, body: Dictionary<String, Any>, callback: CallBack)
    {
        guard let url = URL(string: route) else{
            print("invalid URL, post: \(route)")
            return
        }
        
        Alamofire.request(url, method:.post,parameters: body,encoding: JSONEncoding.default , headers: header)
            .log(level: .all)
            .validate().responseJSON { response in
                switch response.result {
                case .success( _):
                    callback?(JSON(response.result.value ?? ""),true)
                case .failure( _):
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        print("Failure Response: \(json!)")
                        callback?(JSON(response.data!),false)
                    }
                }
        }
    }
    
    
    func delete(route:String, callback:CallBack){
        guard let url = URL(string: route) else{
            print("invalid URL, delete: \(route)")
            return
        }
        
        Alamofire.request(url, method:.delete ,headers: header)
            .log(level: .all)
            .validate().responseJSON { response in
                switch response.result {
                case .success( _): break
                //    onCompletion!(JSON(response.result.value),true)
                case .failure( _):
                    if  response.response?.statusCode == 401 {
                    }else{
                        
                    }
                    
                }
        }
        
        
    }
    
    func get(route:String, callback:GetCallback, onLoginNeeded:@escaping () -> Void)
    {
        
        guard let url = URL(string: route) else{
            print("invalid URL, get: \(route)")
            return
        }
        //let encodedName =  newString.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed)
        
        Alamofire.request(url, method:.get ,headers: header)
            .log(level: .all)
            .validate().responseJSON { response in
                switch response.result {
                case .success( _):
                    let json = JSON(response.result.value ?? "")
                    callback?(json, ResponseStatus.success)
                    break
                case .failure(_ ):
                    if  response.response?.statusCode == 401 {
                        callback?(nil,ResponseStatus.authenticationFailed)
                    }else if response.response?.statusCode == 500{
                        
                        callback?(nil,ResponseStatus.serverError)
                        
                    }else {
                        callback?(nil,ResponseStatus.internetFailed)
                    }
                    //                    print("FuckckckcingREsp",error.localizedDescription)
                    //                    print("teeeeest  ",String(data: response.data!, encoding: String.Encoding.utf8))
                    //
                    //                    print("   response.data",response.data)
                    //                    print("   response.data?.description",response.data?.description)
                    //                    print("   response.error",response.error)
                    //                    print("   response.result.value",response.result.value)
                    //                    print("   response.result.error",response.result.error)
                    //                    print("   response.response",response.data)
                    //
                    //
                    callback?(nil,ResponseStatus.failed)
                    
                }
        }
        
    }
    
    func upload(route:String, data:Data, withName:String, fileName:String, fileType:String, parameters:[String:String]?, callback: CallBack){
        guard let url = URL(string: route) else{
            print("invalid URL, upload: \(route)")
            return
        }
        
        
        Alamofire.upload(multipartFormData: {(multiData) in
            multiData.append(data, withName: withName, fileName: fileName, mimeType: fileType)
            
            if let parameters = parameters{
                for item in parameters{
                    multiData.append(item.key.data(using: .utf8)!, withName: item.value)
                }
            }
        }, usingThreshold: UInt64(), to: url, method: .post, headers: header, encodingCompletion: {(result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    if response.error != nil{
                        callback?(nil, false)
                        return
                    }
                    callback?(nil, true)
                }
            case .failure(let error):
                callback?(nil, false)
                print("Error in upload: \(error.localizedDescription)")
            }
        })
    }
    
    
    func download(route: String, desinition : URL, completion: @escaping (_:Bool) -> ()) {
        
        guard let url = URL(string: route) else{
            print("invalid URL, download: \(route)")
            return
        }
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.httpAdditionalHeaders = header
        
        let session = URLSession(configuration: sessionConfig)
        let request = try! URLRequest(url: url, method: .get)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Success: \(statusCode)")
                }
                
                do {
                    if FileManager.default.fileExists(atPath: desinition.path){
                        try FileManager.default.removeItem(at: desinition)
                    }
                    
                    if let data =  try? Data(contentsOf: tempLocalUrl){
                        _ = FileManager.default.createFile(atPath: desinition.path, contents: data, attributes: nil)
                        //try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                        completion(true)
                    }
                    
                } catch (let writeError) {
                    print("error writing file \(desinition) : \(writeError)")
                    completion(false)
                    do{
                        try FileManager.default.removeItem(at: tempLocalUrl)
                    }catch{}
                }
                
            } else {
                print("download failed: ", error?.localizedDescription as Any);
            }
        }
        
        task.resume()
    }
}



enum ResponseStatus {
    case success
    case failed
    case internetFailed
    case authenticationFailed
    case serverError
}

enum ContentType{
    case JSON
    case UrlEncoded
    case Multipart
}

